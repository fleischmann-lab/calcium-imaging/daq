//Run this script on the secondary Teensy which should receive a copy of the sniff signal on pin A5, to send out an odor trigger on pine A4

#include <PeakDetection.h>                       // import lib

PeakDetection peakDetection;                     // create PeakDetection object
int PrevPeakState = 0;
unsigned long peakTime = 0; // Store the time of peak detection
bool peakHandled = false;   // Flag to indicate if the peak has been handled
const unsigned long delayDuration = 30; // Delay duration in milliseconds
double emaRate = 0;
const double rateAlpha = 0.03; 

void setup() {
  Serial.begin(9600);                            // set the data rate for the Serial communication
  pinMode(A5, INPUT);                            // analog pin used to connect the sensor
  pinMode(A4, OUTPUT);                           // output to trigger based on peak detection
  pinMode(A6, INPUT);
  peakDetection.begin(250, 1, .2);               // sets the lag, threshold and influence

}

void loop() {
    double data = (double)analogRead(A5)/512-1;  // converts the sensor value to a range between -1 and 1
    peakDetection.add(data);                     // adds a new data point
    int peak = peakDetection.getPeak();          // 0, 1 or -1
    int mon = analogRead(A6);
    
  if (peak == -1 && PrevPeakState != peak) {
    peakTime = millis();     // Store the time of peak detection
    peakHandled = false;     // Reset the peak handling flag
    emaRate = rateAlpha + (1 - rateAlpha) * emaRate;
  }
  else {
    emaRate = (1 - rateAlpha) * emaRate;
  }

  if (!peakHandled && millis() - peakTime >= delayDuration) {
    digitalWrite(A4, HIGH);
    peakHandled = true;      // Set the flag to indicate peak has been handled
  } else {
    digitalWrite(A4, LOW);
  }

    PrevPeakState = peak;
        
    Serial.print(data*10);                          // print data
    Serial.print(",");
    Serial.print(peak);                          // print peak status
    Serial.print(",");
//    Serial.print(0.01*mon);                    // print moving average
//    Serial.print(",");
    Serial.println(emaRate*50);                    // print moving rate
    delay(50);

}
