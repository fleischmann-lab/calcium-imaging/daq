#include <PeakDetection.h>                       // import lib

PeakDetection peakDetection;                     // create PeakDetection object
int PrevPeakState = 0;
unsigned long peakTime = 0;                      // Store the time of peak detection
bool peakHandled = false;                        // Flag to indicate if the peak has been handled
const unsigned long delayDuration = 30;          // Delay duration in milliseconds
const unsigned long timeWindow = 10000;          // Time window of 10 seconds for moving average

unsigned long eventTimes[100000];                   // Store up to 100 event times, adjust if necessary
int eventCount = 0;                              // Number of events recorded

void setup() {
  Serial.begin(9600);                            
  pinMode(A5, INPUT);                            // flow sensor signal
  pinMode(A4, OUTPUT);                           // trigger to Main Teensy
  peakDetection.begin(250, 1, .2);               // sets the lag, threshold and influence
}

void loop() {
    double data = (double)analogRead(A5)/512-1;  // converts the sensor value to a range between -1 and 1
    peakDetection.add(data);                     // adds a new data point
    int peak = peakDetection.getPeak();          // 0, 1 or -1
    int negPeakSpike = 0;                        // Reset negative peak spike marker
    
    if (peak == -1 && PrevPeakState != peak) {
      peakTime = millis();                       // Store the time of peak detection
      peakHandled = false;                       // Reset the peak handling flag
      negPeakSpike = 1;                          // Set the spike for negative peak
      recordEvent(millis());                     // Record event time when peak is -1
    }

    if (!peakHandled && millis() - peakTime >= delayDuration) {
      digitalWrite(A4, HIGH);
      peakHandled = true;                        // Set the flag to indicate peak has been handled
    } else {
      digitalWrite(A4, LOW);
    }

    PrevPeakState = peak;

    int movingAverage = calculateMovingAverage(); // Calculate moving average
    Serial.print(peak);                          // print peak status
    Serial.print(",");
    Serial.print(negPeakSpike);                  // print spike for negative-going peak
    Serial.print(",");
    Serial.println(movingAverage);               // print moving average
    delay(10);
}

void recordEvent(unsigned long eventTime) {
    eventTimes[eventCount % 100] = eventTime;    // Use modulo to avoid overflow
    eventCount++;
}

int calculateMovingAverage() {
    int count = 0;
    unsigned long currentTime = millis();
    for (int i = 0; i < min(eventCount, 100); i++) {
        if (currentTime - eventTimes[i] <= timeWindow) {
            count++;
        }
    }
    return count;
}
