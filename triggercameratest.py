from gpiozero import Button
from picamera import PiCamera
from datetime import datetime
from signal import pause

button = Button(19)
camera = PiCamera()

def capture():
    camera.resolution = (640,480)
    camera.framerate = 30
    camera.start_recording('test20200915.h264')
    start = datetime.now()
    while (datetime.now() - start).seconds < 30:
        camera.annotate_text = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    camera.stop_recording()

button.when_released = capture

pause()