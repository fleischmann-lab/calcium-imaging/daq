#include <FlexiTimer2.h>

#define pulsePinMicroscope 0 // 16 LED - green // // 0 Microscope
#define pulseOpenValve 1 // Airflow valve
#define motionpin 35
#define trigPin 13

// decoder map
// a = session on or off
// t = total number of groupings
// d = delay between trials in ms
char knownHeaders[] =    {'a', 't', 'd'};
int32_t knownValues[] = {0, 25, 1000};
int knownCount = 3;

bool sessionOn = 0;

// Timer parameters
elapsedMillis trialTimer;
elapsedMillis olfactoryTimer;
elapsedMillis odorDelayTimer;

// Logical parameters
bool beginNewTrial = 0;             // things we reset between each group of solenoids
bool beginNewOdor = 0;              // things we reset between solenoids
bool triggeredMicroscope = 0;
bool triggeredOlfactory1 = 0;
bool turnOffOlfactory = 0;
bool triggeredAirflow = 0;          // Air flow


// Timing steps
uint32_t olfactoryDelay1 = 5000;   // Pre-stimulus time - 10000
uint32_t olfactoryDuration1 = 2000; // Stimulus time - 1000
uint32_t trialTime = 10000;         // Total duration (add at least one second extra for scope) - 30000
//uint32_t odorDelayTime = 15000;     // Time between odors (give scope time) - 30000

// Counters - Manual
uint32_t odorPinArray[] = {1,2}; // Manual
uint32_t odorPinCount = 2;          // Manual
uint32_t odorCounter = 1;

// Counters - Initialization
uint32_t trialCounter = 0;
uint32_t lastPick = 999;
uint32_t curOdorPin = 0;

// WheelParameters

volatile uint32_t EncoderAngle = 0;
volatile uint32_t PreviousTime = 0;

// sniff trigger Parameters
int snifftrig = 20;
int trig = 0;

// respirator params
long currentRead = 0;
long arrayReads[10];
int curArray = 0;
int numInArray = 10;
long localDeriv = 0;

// PID params
long currentReadPID =0;

// (oStates) Interupt Timing
int sampsPerSecond = 1000;
float evalEverySample = 1.0; // number of times to poll the vStates

void setup() {
  analogReadResolution(12);
  //CHECK
  Serial.begin(9600);
  delay(2000);
//  Serial.println("Hello");
  for (int i = 0; i < odorPinCount; i++) {
    pinMode(odorPinArray[i], OUTPUT);
    digitalWrite(odorPinArray[i], LOW);
    attachInterrupt (motionpin, rising, RISING);
    while (!Serial);
  }
  pinMode(trigPin, INPUT);
  pinMode(pulsePinMicroscope, OUTPUT);
  digitalWrite(pulsePinMicroscope, LOW);
  pinMode(pulseOpenValve, OUTPUT);   // Air flow valve
  digitalWrite(pulseOpenValve, LOW); // Air flow valve
  beginNewOdor = 0;
  beginNewTrial = 1;
  odorCounter = 0;

  pinMode(snifftrig, INPUT);

  // Start Program Timer
  FlexiTimer2::set(1, evalEverySample / sampsPerSecond, oStates);
  FlexiTimer2::start();

//  Serial.println("Done with setup");

}
void loop() {
  // nope we are l33t
}

bool EXP_BEGUN = false; 

void oStates() {
  if (knownValues[0] == 1) {
    if (trialCounter < knownValues[1]) {

      int curSerVar = flagReceive(knownHeaders, knownValues);
      if (curArray >= numInArray) {
        curArray = 0;
      }
      currentRead = analogRead(A3);
      trig = digitalRead(snifftrig);
      arrayReads[curArray] = currentRead;
//      localDeriv = arrayReads[3] - arrayReads[0];
      currentReadPID = analogRead(A0);
      if (EXP_BEGUN) {
        dataReport();
      } else {
        EXP_BEGUN = true;
      }
      curArray = curArray + 1;
      

      /*
          A "trial" is a group of channels. But, each channel run is a useful trial to the
         experimenter. The logic here is to allow the experimenter to only care about the
         number of odors and how often they want to assay them.
      */
      if (beginNewTrial == 1) {
//
//        Serial.println("About to pick");
        scrambleArray(odorPinArray, odorPinCount, 1);
        /*
           If the last array element is equal to the first element of the follwing array,
           rescramble the array.
        */
        while (odorPinArray[0] == lastPick) {
          scrambleArray(odorPinArray, odorPinCount, 1);
        }
        curOdorPin = 0;
        // We log the last channel picked for the trial.
        lastPick = odorPinArray[odorPinCount - 1];
        beginNewTrial = 0;
      }

      // Now we reset the things that happen between odors.
      if (beginNewOdor == 0) {
        trialTimer = 0;
        olfactoryTimer = 0;
        triggeredMicroscope = 0;
        turnOffOlfactory = 0;
        triggeredOlfactory1 = 0;
        beginNewOdor = 1;
        odorCounter = odorCounter + 1;

//        Serial.print("new odor # ");
//        Serial.println(odorPinArray[curOdorPin]);
      }

      // Condition 2
      /*
         If selectedValve == open channel, do not close the valve during the olfactory time
         and do not open any valve.
      */
      // if we just start a trial, trigger the test scope (LED)
      if (triggeredMicroscope == 0) {

//        Serial.println("will trigger");
        digitalWrite(pulsePinMicroscope, HIGH);
        digitalWrite(pulseOpenValve, HIGH);  // Air flow valve
//        Serial.println("Trig microscope");
//        Serial.println("Open air flow");
//        Serial.println(trialTimer);
        triggeredMicroscope = 1;
        triggeredAirflow = 1;                // Air flow valve
      }
    if ((trialTimer >= olfactoryDelay1) && (triggeredOlfactory1 == 0) && (trig == HIGH)) {
//      if ((trialTimer >= olfactoryDelay1) && (triggeredOlfactory1 == 0)) {
         
        digitalWrite(pulseOpenValve, LOW);   // Air flow valve
        triggeredAirflow = 0; // New
//        Serial.print("Debug our pin address ");
//        Serial.println(odorPinArray[curOdorPin]);

        digitalWrite(odorPinArray[curOdorPin], HIGH);
        olfactoryTimer = 0;
        triggeredOlfactory1 = 1;
//        Serial.println("Air flow OFF");
//        Serial.println("Trig OLFACT");
//        Serial.println(trialTimer);
        turnOffOlfactory = 1;
      }

      if ((olfactoryTimer >= olfactoryDuration1) && (turnOffOlfactory == 1)) {
        digitalWrite(odorPinArray[curOdorPin], LOW);
//        Serial.println("OLFACT OFF");
//        Serial.println("Air flow ON");
        Serial.println(trialTimer);
        turnOffOlfactory = 0;
        digitalWrite(pulseOpenValve, HIGH);  // Air flow valve
        triggeredAirflow = 1; // New
      }

      if (trialTimer >= trialTime) {

//        Serial.println("Finished odor; ");
//        Serial.print("waiting for the next odor; pausing for ");
        Serial.println(knownValues[2]);
        delay(knownValues[2]);
        curOdorPin++;
        if (curOdorPin > odorPinCount - 1) {
          trialCounter = trialCounter + 1;
          beginNewTrial = 1;
//          Serial.print("starting group # ");
          Serial.println(trialCounter + 1);
        }

        digitalWrite(pulsePinMicroscope, LOW);
        // digitalWrite(pulseOpenValve,LOW); // Don't close air flow valve
        Serial.println(trialTimer);

        trialTimer = 0;
        olfactoryTimer = 0;
        beginNewOdor = 0;
        triggeredOlfactory1 = 0;
        triggeredMicroscope = 0;
        // triggeredAirflow = 0;            // Don't close air flow valve

      }
    }
    else {
      knownValues[0] = 0;
    }
  }

  else {
    trialCounter = 0;
    odorCounter = 0;
    int curSerVar = flagReceive(knownHeaders, knownValues);
    delay(100);
  }
}

void scrambleArray(uint32_t * array, uint32_t size, bool debugFlag) {
  randomSeed(analogRead(0));
  int last = 0;
  int temp = array[last];
  for (int i = 0; i < size; i++) // Here
  {
    int index = random(size);
    array[last] = array[index];
    last = index;
  }
  array[last] = temp;

  if (debugFlag == 1) {
    for (int i = 0; i < size; i++)
    {
//      Serial.println(odorPinArray[i]);
    }
//    Serial.println("----");

  }
}

void rising ()  {
  attachInterrupt (motionpin, falling, FALLING);
  PreviousTime = micros();
}

void falling ()  {
  attachInterrupt (motionpin, rising, RISING);
  EncoderAngle = micros() - PreviousTime;
}

int flagReceive(char varAr[], int32_t valAr[]) {
  static byte ndx = 0;
  char endMarker = '>';
  char feedbackMarker = '<';
  char rc;
  uint32_t nVal;
  const byte numChars = 32;
  char writeChar[numChars];
  int selectedVar = 0;
  static boolean recvInProgress = false;
  bool newData = 0;
  int32_t negScale = 1;

  while (Serial.available() > 0 && newData == 0) {
    rc = Serial.read();

    if (recvInProgress == false) {
      for ( int i = 0; i < knownCount; i++) {
        if (rc == varAr[i]) {
          selectedVar = i;
          recvInProgress = true;
        }
      }
    }

    else if (recvInProgress == true) {
      if (rc == endMarker ) {
        writeChar[ndx] = '\0'; // terminate the string
        recvInProgress = false;
        ndx = 0;
        newData = 1;
        nVal = int32_t(String(writeChar).toInt());
        valAr[selectedVar] = nVal * negScale;
        return selectedVar;
      }

      else if (rc == feedbackMarker) {
        writeChar[ndx] = '\0'; // terminate the string
        recvInProgress = false;
        ndx = 0;
        newData = 1;
        Serial.print("echo");
        Serial.print(',');
        Serial.print(varAr[selectedVar]);
        Serial.print(',');
        Serial.print(valAr[selectedVar]);
        Serial.print(',');
        Serial.println('~');
      }
      else if (rc == '-') {
        negScale = -1;
      }
      else if (rc != feedbackMarker || rc != endMarker) {
        writeChar[ndx] = rc;
        ndx++;
        if (ndx >= numChars) {
          ndx = numChars - 1;
        }
      }
    }
  }
}

void dataReport() {
//  Serial.print("oData");
//  Serial.print(',');
  Serial.print(trialTimer);
  Serial.print(',');
  Serial.print(odorPinArray[curOdorPin]);
  Serial.print(',');
//  Serial.print(localDeriv);
//  Serial.print(',');
  Serial.print(currentRead);
  Serial.print(',');
  Serial.print(EncoderAngle);
  Serial.print(',');
  Serial.print(odorCounter);
  Serial.print(',');
  Serial.print(triggeredOlfactory1 * turnOffOlfactory); // print odorON time
  Serial.print(",");
  Serial.println(trig);
//  Serial.print(',');
//  Serial.println(currentReadPID);
}
